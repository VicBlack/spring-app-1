package ru.kupriyanov.springcourse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        TestBean testBean = context.getBean("testBean", TestBean.class);
        TestBean testBean2 = context.getBean("testBean2", TestBean.class);
        System.out.println(testBean.getName() + " " + testBean2.getName());
        context.close();
    }

}